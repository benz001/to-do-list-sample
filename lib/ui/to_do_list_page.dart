import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:to_do_list_sample/bloc/event/to_do_list_event.dart';
import 'package:to_do_list_sample/bloc/handle_local/to_do_list_bloc.dart';

class ToDoListPage extends StatefulWidget {
  const ToDoListPage({Key? key}) : super(key: key);

  @override
  State<ToDoListPage> createState() => _ToDoListPageState();
}

class _ToDoListPageState extends State<ToDoListPage> {
  final TextEditingController _textEditingController =
      TextEditingController(text: '');

  @override
  void initState() {
    context.read<ToDoListBloc>().add(GetList());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('To do List Sample'),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.grey[300],
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Container(
                  width: double.infinity,
                  height: 50,
                  color: Colors.white,
                  child: TextField(
                    controller: _textEditingController,
                    decoration:
                        const InputDecoration(hintText: 'Masukkan Task'),
                  ),
                ),
              ),
              BlocConsumer<ToDoListBloc, List<Map<String, dynamic>>>(
                  listener: (context, state) {
                print('state is $state');
              }, builder: (context, state) {
                return ListView.builder(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: state.length,
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    itemBuilder: (context, index) {
                      return _buildItem(state[index]);
                    });
              }),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (_textEditingController.text.isNotEmpty) {
            setState(() {}); // lebih direkomendesaikan pakai value notifier
            context
                .read<ToDoListBloc>()
                .add(AddList(task: _textEditingController.text));
            _textEditingController.clear();
          } else {
            final snackBar = SnackBar(
              content: const Text('Silahkan masukkan task'),
              action: SnackBarAction(
                label: 'Ok',
                onPressed: () {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                },
              ),
            );

            // Find the ScaffoldMessenger in the widget tree
            // and use it to show a SnackBar.
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget _buildItem(Map<String, dynamic> state) {
    return Card(
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(10),
        color: Colors.white,
        margin: const EdgeInsets.only(top: 10),
        child: Row(
          children: [
            Expanded(child: Text(state['task'])),
            GestureDetector(
                onTap: () {
                  print('uuid: ${state['uuid']}');
                  context
                      .read<ToDoListBloc>()
                      .add(DeleteItemList(uuid: state['uuid']));
                  setState(() {});
                },
                child: const Icon(Icons.delete))
          ],
        ),
      ),
    );
  }
}
