import 'package:equatable/equatable.dart';

abstract class ToDoListEvent extends Equatable {}

class GetList extends ToDoListEvent {
  @override
  List<Object?> get props => [];
}

class AddList extends ToDoListEvent {
  final String task;
  AddList({required this.task});
  @override
  List<Object?> get props => [task];
}


class DeleteItemList extends ToDoListEvent {
  final String uuid;
  DeleteItemList({required this.uuid});
  @override
  List<Object?> get props => [uuid];
}
