import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:to_do_list_sample/bloc/event/to_do_list_event.dart';
import 'package:uuid/uuid.dart';

class ToDoListBloc extends Bloc<ToDoListEvent, List<Map<String, dynamic>>> {
  final List<Map<String, dynamic>> _myListTask = [];
  final uuid = const Uuid();

  ToDoListBloc() : super([]) {
    on<GetList>((event, emit) {
      emit(_myListTask);
    });
    on<AddList>((event, emit) {
      _myListTask.add({"uuid": uuid.v1(), "task": event.task});
      emit(_myListTask);
    });

    on<DeleteItemList>((event, emit) {
      _myListTask.removeWhere((element) => element["uuid"] == event.uuid);
    });
  }
}
